package interactions;

import org.junit.*;
import pages.ProductPage;

public class ProductInteractions extends ProductPage {

	public void assertBtnAddCartExists() {
		btnAddCart.isDisplayed();

	}

	public void assertProductTitle(String ProductTitle) {
		Assert.assertEquals(ProductTitle.toString(), productTitle.getText());

	}

	public void clickAddtoCart() {
		btnAddCart.click();

	}

}
