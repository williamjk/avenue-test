package interactions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import pages.IndexPage;

public class IndexInteractions extends IndexPage {

	public void clickCategory() {
		itemMenuCookware.click();
		
	}

	public void assertPage(WebDriver driver, String url) {
		Assert.assertTrue(url.toString().equalsIgnoreCase(driver.getCurrentUrl().toString()));
		
	}

	public void searchProduct(String product) {
		textSearch.clear();
		textSearch.sendKeys(product);
		btnSearch.click();
		
	}

}
