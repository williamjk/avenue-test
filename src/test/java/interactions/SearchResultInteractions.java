package interactions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.SearchResultPage;
import utils.LibraryUtils;

public class SearchResultInteractions extends SearchResultPage {
	

	public void assertPageIsResultSearch() {
		textSearchResultFor.isDisplayed();
		
	}

	public void moveMouseArrowToProductImg(WebDriver driver) {
		LibraryUtils libraryUtils = PageFactory.initElements(driver,
				LibraryUtils.class);
		libraryUtils.scrollToElement(productThumb, driver);

		
	}

	public void clickQuickLookButton() {
		textQuicklookLink.click();
		
	}

	public String storePriceProductClicked() {
		return productPriceClicked.getText();
	}

	public String storeTitleProductClicked() {
		
		return productNameClicked.getText();
	}

	public void assertTitleAndProductPriceisSame(String productPriceClicked,
			String productNameClicked) {
		Assert.assertEquals(productPriceClicked, productPriceInSTheOverlay.getText());
		Assert.assertEquals(productNameClicked, productNameInSTheOverlay.getText());
	}
	
	

}
