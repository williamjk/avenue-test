package interactions;

import org.junit.Assert;

import pages.AddToCartOverlayPage;

public class AddToCartOverlayInteractions extends AddToCartOverlayPage {

	public void assertCartOverlayPageExists() {
		Assert.assertEquals(
				"You've just added the following to your basket:",
				pageTitle.getText());

	}

	public void assertProductNameAdded(String productName) {
		Assert.assertEquals(productName.toString(), productTitle.getText());

	}

	public void assertBtnCheckoutExists() {
		btnCheckout.isDisplayed();

	}

	public void clickBtnCheckout() {
		btnCheckout.click();
		
	}

}
