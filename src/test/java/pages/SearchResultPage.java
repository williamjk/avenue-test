package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.LibraryUtils;

public class SearchResultPage extends LibraryUtils {

  @FindBy(xpath = "//*[@id='results-summary']/h3")
  protected WebElement textSearchResultFor;

  @FindBy(className = "quicklook-link")
  protected WebElement textQuicklookLink;
  

  @FindBy(xpath = "//*[@id='content']/div[2]/ul/li[1]/div[1]/a[1]/span/img")
  protected WebElement productThumb;
  
  @FindBy(xpath = "//*[@id='content']/div[2]/ul/li[1]/a/span")
  protected WebElement productNameClicked;
  
  @FindBy(xpath = "//*[@id='content']/div[2]/ul/li[1]/span/span[2]/span[2]/span[2]")
  protected WebElement productPriceClicked;

  @FindBy(xpath = "//*[@id='purchasing-container']/div[2]/div/h1")
  protected WebElement productNameInSTheOverlay;
  
  @FindBy(xpath = "//*[@id='itemselection']/div/section/section/div/div/div/div[2]/span/span[2]/span[2]/span[2]")
  protected WebElement productPriceInSTheOverlay;


}
