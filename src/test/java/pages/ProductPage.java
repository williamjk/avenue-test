package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.LibraryUtils;

public class ProductPage extends LibraryUtils {

  @FindBy(xpath = "//*[@id='pip']/div[1]/div[7]/div[2]/div[2]/section/div/div/fieldset[1]/button")
  protected WebElement btnAddCart;
  
  @FindBy(xpath = "//*[@id='pip']/div[1]/div[7]/div[2]/div[2]/h1")
  protected WebElement productTitle;

}
