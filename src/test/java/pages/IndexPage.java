package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.LibraryUtils;

public class IndexPage extends LibraryUtils {

	@FindBy(className = "topnav-cookware")
	protected WebElement itemMenuCookware;

	@FindBy(id = "search-field")
	protected WebElement textSearch;

	@FindBy(id = "btnSearch")
	protected WebElement btnSearch;

}
