package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.LibraryUtils;

public class CategoryPage extends LibraryUtils {

	@FindBy(xpath = "//*[@id='super-category']/div[1]/div[1]/ul[2]/li[1]/a")
	protected WebElement itemMenuCookwareSets;
}
