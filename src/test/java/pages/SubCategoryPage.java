package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.LibraryUtils;

public class SubCategoryPage extends LibraryUtils {

	@FindBy(xpath = "//*[@id='subCatListContainer']/ul/li[1]/a/span")
	protected WebElement productItemGreenPanBlackCeramicNonstick12PieceCookwareSet;
	
	@FindBy(xpath = "//*[@id='subCatListContainer']/ul/li[1]/div/a[1]/span/img")
	protected WebElement productIMGGreenPanBlackCeramicNonstick12PieceCookwareSet;
}
