package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.LibraryUtils;

public class AddToCartOverlayPage extends LibraryUtils {

	@FindBy(className = "overlayTitle")
	protected WebElement pageTitle;

	@FindBy(xpath = "//*[@id='title']")
	protected WebElement productTitle;
	
	@FindBy(id = "anchor-btn-checkout")
	protected WebElement btnCheckout;

}
