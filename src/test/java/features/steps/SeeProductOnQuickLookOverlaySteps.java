package features.steps;

import interactions.IndexInteractions;
import interactions.SearchResultInteractions;

import org.openqa.selenium.support.PageFactory;

import utils.DriverTestInstance;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SeeProductOnQuickLookOverlaySteps extends DriverTestInstance {

	IndexInteractions indexInteractions = PageFactory.initElements(driver,
			IndexInteractions.class);
	SearchResultInteractions searchResultInteractions = PageFactory.initElements(driver,
			SearchResultInteractions.class);

	protected String productPriceClicked = null;
	protected String productNameClicked = null;
	
	@Given("^I search for a product \"([^\"]*)\" using input on the top right area,$")
	public void I_search_for_a_product_using_input_on_the_top_right_area(
			String product) throws Throwable {
		indexInteractions.searchProduct(product);
	}

	@Then("^Search field takes to the results page$")
	public void Search_field_takes_to_the_results_page() throws Throwable {
		searchResultInteractions.assertPageIsResultSearch();
	}

	@Then("^Hover the product's image display the quick look button$")
	public void Hover_the_product_s_image_display_the_quick_look_button()
			throws Throwable {
		searchResultInteractions.moveMouseArrowToProductImg(driver);
	}

	@Then("^Click the quick look button show the product overlay$")
	public void Click_the_quick_look_button_show_the_product_overlay()
			throws Throwable {
		productPriceClicked = searchResultInteractions.storePriceProductClicked();
		productNameClicked = searchResultInteractions.storeTitleProductClicked();
		searchResultInteractions.clickQuickLookButton();
	}

	@Then("^The product clicked should have the same name and price as the product in the overlay$")
	public void The_product_clicked_should_have_the_same_name_and_price_as_the_product_in_the_overlay()
			throws Throwable {
		searchResultInteractions.assertTitleAndProductPriceisSame(productPriceClicked,productNameClicked);
	}
}
