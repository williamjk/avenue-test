package features.steps;

import interactions.AddToCartOverlayInteractions;
import interactions.CartInteractions;
import interactions.CategoryInteractions;
import interactions.IndexInteractions;
import interactions.ProductInteractions;
import interactions.SubCategoryInteractions;

import org.openqa.selenium.support.PageFactory;

import utils.DriverTestInstance;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddProductToCartSteps extends DriverTestInstance {

	IndexInteractions indexInteraction = PageFactory.initElements(driver,
			IndexInteractions.class);
	CategoryInteractions categoryInteractions = PageFactory.initElements(
			driver, CategoryInteractions.class);
	SubCategoryInteractions subcategoryInteractions = PageFactory.initElements(
			driver, SubCategoryInteractions.class);
	ProductInteractions productInteractions = PageFactory.initElements(driver,
			ProductInteractions.class);
	AddToCartOverlayInteractions addToCartOverlay = PageFactory.initElements(
			driver, AddToCartOverlayInteractions.class);
	CartInteractions cartInteraction = PageFactory.initElements(driver,
			CartInteractions.class);

	@Given("^i stay on the ecommerce page \"([^\"]*)\".$")
	public void i_stay_on_the_ecommerce_page_(String urlPage) throws Throwable {
		driver.get(urlPage);
		indexInteraction.assertPage(driver, urlPage);
	}

	@Given("^access category \"([^\"]*)\" in menu.$")
	public void access_category_in_menu(String category) throws Throwable {
		indexInteraction.clickCategory();
	}

	@Given("^access subcategory \"([^\"]*)\" in menu.$")
	public void access_subcategory_in_menu(String subcategory) throws Throwable {
		categoryInteractions.clickSubcategory(subcategory);
	}

	@Given("^click on product \"([^\"]*)\".$")
	public void click_on_product_(String product) throws Throwable {
		subcategoryInteractions.clickProduct(product);
	}

	@Then("^Product page shows Add to cart button.$")
	public void Product_page_shows_Add_to_cart_button() throws Throwable {
		productInteractions.assertBtnAddCartExists();
	}

	@Then("^Add product \"([^\"]*)\" to cart.$")
	public void Add_product_to_cart(String ProductTitle) throws Throwable {
		productInteractions.assertProductTitle(ProductTitle);
		productInteractions.clickAddtoCart();
	}

	@When("^click on Add to cart button, add to cart overlay appears.$")
	public void click_on_Add_to_cart_button_add_to_cart_overlay_appears()
			throws Throwable {
		addToCartOverlay.assertCartOverlayPageExists();
	}

	@Then("^The product \"([^\"]*)\" you added to cart should be on shopping cart page.$")
	public void The_product_you_added_to_cart_should_be_on_shopping_cart_page(
			String productName) throws Throwable {
		addToCartOverlay.assertProductNameAdded(productName);
	}

	@When("^Checkout button is on the add to cart overlay.$")
	public void Checkout_button_is_on_the_add_to_cart_overlay()
			throws Throwable {
		addToCartOverlay.assertBtnCheckoutExists();
	}

	@When("^click on Checkout button.$")
	public void click_on_Checkout_button() throws Throwable {
		addToCartOverlay.clickBtnCheckout();
	}

	@Then("^shopping cart page is shown.$")
	public void shopping_cart_page_is_shown() throws Throwable {
		cartInteraction.assertThisIsCartPage(driver);
	}

}
