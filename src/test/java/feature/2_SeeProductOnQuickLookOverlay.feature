Feature: See a Product on Quick Look Overlay
  As a customer
  I want to search for a product and open it's quick look overlay
  I should be able to see the exact same clicked product on the quick look overlay

  Scenario: Saerch product and see a Product on Quick Look Overlay
    Given i stay on the ecommerce page "http://www.williams-sonoma.com/".
    Given I search for a product "fry pan" using input on the top right area,
    Then Search field takes to the results page
    And Hover the product's image display the quick look button
    And Click the quick look button show the product overlay
    Then The product clicked should have the same name and price as the product in the overlay